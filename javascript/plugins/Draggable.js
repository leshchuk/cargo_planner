window.App = window.App || {};
App.plugin = App.plugin || {};

App.plugin.Draggable = function(options) {
    var $this = this;
    var defFn = function() {return false;};

    options = options || {};

    Object.assign(this, {
        container: document.body,
        startDrag: defFn,
        validator: defFn,
        success: defFn,
        reject: defFn,
        scope: this
    }, options);

    if (!this.element) {
        throw "[Plugin Draggable]: the source element is not defined.";
    }
 
    var delta;

    var startDrag = function(evt) {
            $this.start = pos($this.element);
            $this.position = pos(evt);
            delta = vec.sub($this.start, $this.position);

            $this.element.classList.add("dragging")
 
            document.addEventListener("mousemove", doDrag, false);
            document.addEventListener("mouseup", endDrag, false);

            $this.startDrag.call($this.scope, $this.position);
 
            // document.body.style.cursor = "crosshair";
 
        },
 
        doDrag = function(evt) {

            $this.position = pos(evt);
            var p = vec.add($this.position, delta);
            $this.element.style.left = p.x + "px";
            $this.element.style.top = p.y + "px";
            $this.markValid(
                $this.validator.call($this.scope, $this.position)
            );

        },
 
        endDrag = function(evt) {
 
            $this.position = pos(evt);
            var valid = $this.validator.call($this.scope, $this.position);

            if (valid) {
                $this.success.call($this.scope, $this.position);
            }
            else {
                $this.reject.call($this.scope, $this.start);
            }

            $this.element.classList.remove("dragging")

            document.removeEventListener("mousemove", doDrag, false);
            document.removeEventListener("mouseup", endDrag, false);
 
            // document.body.style.cursor = "default";
 
        };
 
    this.element.addEventListener("mousedown", startDrag, false);
};
 
App.plugin.Draggable.prototype = {
    valid: function() {
        return this.validator(this.position);
    },
 
    markValid: function(state) {
        this.element.classList[state === true ? "add" : "remove"]("valid");
    }
};

var pos = function(v) {
    if (v instanceof Event) {
        v = vec.sub({x: v.clientX, y: v.clientY}, pos(App.workarea.el));
    }
    else {
        v = {x: v.offsetLeft, y: v.offsetTop};
    }
    return v;
};

var vec = {
    add: function(v1, v2) {
        return {x: v1.x + v2.x, y: v1.y + v2.y}
    },
    sub: function(v1, v2) {
        return {x: v1.x - v2.x, y: v1.y - v2.y}
    }
};
