window.UTILS = {

    /**
     * @static
     * Создает элемент DOM с заданными параметрами.
     * Опционально элемент создается с потомками, может прикрепляться к родителю.
     * Элементу могут быть назначены обработчики событий.
     *
     * @param {Object}          [data]
     * @param {string}          [data.tag = "div"] имя тега создаваемого элемента.
     * @param {string|string[]} [data.cls] классы css - строка, строка с разделителями (пробел или ,), массив строк.
     * @param {Object}          [data.attrs] атрибуты создаваемого элемента.
     * @param {string}          [data.text] содержимое текстового узла внутри создаваемого элемента.
     * @param {string}          [data.html] форматированная разметка внутреннего HTML элемента.
     * @param {DOMElement}      [data.parent] родительский элемент.
     * @param {DOMElement|DOMElement[]|Object|Object[]} [data.nodes] дочерний DOM элемент (или массив дочерних элементов),
     * или конфигурационный объект (или массив) для рекурсивного вызова метода {DDF.UI.Dom#node}.
     * @param {Object}          [data.listeners] обработчики событий создаваемого элемента.
     *
     * @return {DOMElement} возвращает созданный элемент.
     */
    node: function(data) {
        data = Object.assign({}, {
            tag: "div",
            cls: [],
            attrs: {},
            text: null,
            html: null,
            parent: null,
            nodes: [],
            listeners: {},
            dataset: {}
        }, data);

        var node = document.createElement(data.tag);
        var i, l;

        if (data.id) {
            node.id = data.id;
        }

        if (data.name) {
            if (node.name !== undefined) {
                node.name = data.name;
            }
            else {
                node.setAttribute("name", data.name);
            }
        }

        data.cls = this.array(data.cls);
        for (i = 0, l = data.cls.length; i < l; i++) {
            var c = data.cls[i];
            if (c) node.classList.add(c);
        }

        if (data.style) {
            Object.assign(node.style, data.style);
        }

        for (var item in data.dataset) {
            if (data.dataset.hasOwnProperty(item)) {
                node.setAttribute("data-" + item, data.dataset[item]);
            }
        }

        for (var attr in data.attrs) {
            if (data.attrs.hasOwnProperty(attr)) {
                node.setAttribute(attr, data.attrs[attr]);
            }
        }


        for (var evt in data.listeners) {
            if (data.listeners.hasOwnProperty(evt)) {
                node.addEventListener(evt, data.listeners[evt], false);
            }
        }

        if (data.html) {
            node.innerHTML = data.html;
        }

        if (data.text) {
            node.appendChild(document.createTextNode(data.text));
        }

        if (!(data.nodes instanceof Array)) {
            data.nodes = [ data.nodes ];
        }

        for (i = 0, l = data.nodes.length; i < l; i++) {
            if (data.nodes[i] instanceof HTMLElement) {
                node.appendChild(data.nodes[i]);
            }
            else if (data.nodes[i] instanceof Object) {
                node.appendChild(this.node(data.nodes[i]));
            }
        }

        if (data.parent) {
            data.parent.appendChild(node);
        }

        return node;
    },

    fieldContainer: function(options) {
        options = Object.assign({
            label: "",
            name: typeof options == "string" ? options : "",
            type: "text",
            value: ""
        }, typeof options == "string" ? {} : options);
        return this.node({
            cls: "field-container flex-container",
            nodes: [{
                tag: "label",
                text: options.label,
                attrs: {"for": options.name}
            }, {
                tag: "input",
                name: options.name,
                attrs: Object.assign({type: options.type, value: options.value}, options.attrs),
                listeners: options.listeners
            }]
        })
    },

    table: function(data) {
        return this.node({
            tag: "table",
            parent: data.parent,
            id: data.id,
            cls: data.cls,
            listeners: data.listeners,
            nodes: [
                this.thead(data.headers),
                this.tbody(data.data, data.fields)
            ]
        });
    },
 
    thead: function(arr) {
        if (!arr) return null;
        arr = this.array(arr);
        return this.node({
            tag: "thead",
            nodes: [
                {
                    tag: "tr",
                    nodes: arr.map(function(v) {
                        var match = v.match(/^(.+)#(\d+)(%?)$/);
                        if (match) {
                            return {tag: "th", text: match[1], attrs: {style: "width: " + match[2] + (match[3] || "px") + ";"}};
                        }
                        return {tag: "th", text: v};
                    })
                }
            ]
        });
    },
 
    tbody: function(data, fields) {
        var rows = [], i;
        for (i = 0; i < data.length; i++) {
            var row = data[i];
            if (!(row instanceof HTMLElement)) row = this.trow(row, fields);
            rows.push(row);
        }
        return this.node({
            tag: "tbody",
            nodes: rows
        });
    },
 
    trow: function(rec, fields) {
        var cells = [], i;
        if (rec instanceof Array) {
            for (i = 0; i < rec.length; i++) {
                cells.push(this.tcell(rec[i]));
            }
        }
        else if (rec instanceof Object) {
            if (fields && fields.length) {
                for (i = 0; i < fields.length; i++) {
                    cells.push(this.tcell(rec[fields[i]]));
                }
            }
        }
        return this.node({tag: "tr", nodes: cells});
    },
 
    tcell: function(data) {
        if (typeof data == "string" || typeof data == "number") {
            data = "" + data;
            var cls = [];
            var match = data.match(/^([^#]*)?(#.+)$/);
            if (match) {
                cls = match[2].substr(1).split(/[\s,]+/g);
                data = match[1] || "";
            }
            if (/^[\$\-\+]?\d+(\.\d+)?(\s*(?:kg|m|mm|cm))?$/.test(data)) {
                cls.push("text-right");
            }
            return this.node({tag: "td", text: data, cls: cls});
        }
        return this.node({tag: "td", nodes: data, cls: cls});
    },
 
    array: function(value) {
        if (typeof value == "string") value = value.split(/[\s,]+/g);
        else if (value === undefined) value = [];
        else if (!(value instanceof Array)) value = [ value ];
        return value;
    },

    bool: function(val) {
        return val == 1 || val == "true" || val === true;
    },

    random: function(from, to) {
        from = from || 0;
        to = Math.max(from + 1, to || 10);
        return from + Math.round(Math.random() * (to - from));
    }
};

