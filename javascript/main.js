
var onCHSettingsChange = function(e) {
	console.log(e.target.value);
};

var onAddUnit = function(e) {
	var m = /(\d)\D+(\d)/.exec(e.target.innerText);
	App.workarea.addUnit(parseInt(m[1]), parseInt(m[2]));
};

UTILS.node({
	cls: "flex-container flex-rows",
	id: "page",
	parent: document.body,
	listeners: {
		mousedown: function(e) {
			Array.from(document.getElementsByClassName("in")).forEach(function(el) {
				el.classList.remove("in");
			});
		}
	},
	nodes: [{
		id: "page-header",
		text: "Cargo Planner"
	}, {
		id: "page-body",
		cls: "flex-container",
		nodes: [{
			id: "dashboard",
			cls: "flex-container flex-rows",
			nodes: [{
				cls: "dashboard_panel",
				id: "cargo-hold_settings",
				nodes: [{
					cls: "dashboard_panel-header",
					text: "Cargo hold settings"
				}, {
					cls: "dashboard_panel-body",
					nodes: [
						UTILS.fieldContainer({
							name: "cargo-hold-width",
							label: "Width",
							type: "number",
							value: 2,
							attrs: {min: 1, max: 5, step: 1},
							listeners: {
								change: onCHSettingsChange
							}
						}), 
						UTILS.fieldContainer({
							name: "cargo-hold-length",
							label: "Length",
							type: "number",
							value: 5,
							attrs: {min: 1, max: 10, step: 1},
							listeners: {
								change: onCHSettingsChange
							}
						})
					]
				}]
			}, {
				cls: "dashboard_panel",
				id: "cargo-list",
				nodes: [{
					cls: "dashboard_panel-header",
					text: "Cargo list",
					nodes: [{
						cls: "btn",
						text: "+",
						listeners: {
							click: function(e) {
								e.stopPropagation();
								e.target.nextSibling.classList.toggle("in");
							}
						}
					}, {
						tag: "ul",
						cls: "drop-down",
						nodes: [{
							tag: "li",
							text: "Unit 1 x 1",
							listeners: {
								mousedown: onAddUnit
							}
						}, {
							tag: "li",
							text: "Unit 1 x 2",
							listeners: {
								mousedown: onAddUnit
							}
						}, {
							tag: "li",
							text: "Unit 2 x 1",
							listeners: {
								mousedown: onAddUnit
							}
						}, {
							tag: "li",
							text: "Unit 2 x 2",
							listeners: {
								mousedown: onAddUnit
							}
						}]
					}]
				}, {
					cls: "dashboard_panel-body",
					nodes: UTILS.table({
						headers: "ID,Units#80%,Position#20%",
						fields: ["id", "name", "pos"],
						data: []
					})
				}]
			}]
		}, {
			id: "workarea",
			cls: "flex-container flex-rows"
		}]
	}, {
		id: "page-footer"
	}]
});

App.workarea = new App.Workarea();

App.hold = new App.CargoHold();
App.hold.init(App.workarea.tile(1, 1));

