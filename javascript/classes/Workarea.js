window.App = window.App || {};

App.Workarea = function() {
	this.tiles = [];
	this.units = [];

	this.el = document.getElementById("workarea");
	var len = Math.floor(this.el.clientHeight / 12);
	var xCount = Math.floor(this.el.clientWidth / len);

	for (var y = 0; y < 12; y++) {
		var row = UTILS.node({
			cls: "row flex-container",
			parent: this.el
		});
		var arr = [];
		for (var x = 0; x < xCount; x++) {
			arr.push(new App.Tile(x, y, row));
		}
		this.tiles.push(arr);
	}

}

App.Workarea.prototype = {
	tile: function(x, y) {
		return this.tiles[y][x];
	},

    tileByPos: function(pos) {
        var t = this.tile(0, 0);
        var x = Math.floor(pos.x / t.el.clientWidth);
        var y = Math.floor(pos.y / t.el.clientHeight);
        return this.tile(x, y);
    },

	addUnit: function(width, length) {
		this.units.push(new App.Unit(width, length))
	}
}