window.App = window.App || {};

App.Tile = function(x, y, row) {
    var $this = this;

	this.x = x;
	this.y = y;

	this.el = UTILS.node({
		cls: "cell",
		parent: row,
		dataset: {x: x, y: y},
        // listeners: {
        //     mouseover: function() {
        //         App.workarea.actveTile = $this;
        //     },
        //     mouseout: function() {
        //         // App.workarea.actveTile = null;
        //     }
        // }
	});
}

App.Tile.prototype = {
	set hold(state) {
		this._hold = UTILS.bool(state);
		this.el.classList[this._hold ? "add" : "remove"]("hold_cell");
	},
	get hold() {
		return UTILS.bool(this._hold);
	},

    set occupied(state) {
        this._occupied = UTILS.bool(state);
        this.el.classList[this._occupied ? "add" : "remove"]("occupied");
    },
    get occupied() {
        return UTILS.bool(this._occupied);
    },

	get x() {
		return parseInt(this.el.dataset.x, 10);
	},
	get y() {
		return parseInt(this.el.dataset.y, 10);
	},
    get idx() {
        return this.x + "x" + this.y;
    }

	// _onMouseOver: function(e) {
	// 	var t = e.target;
	// 	console.log("over cell(" + t.dataset.x + ", " + t.dataset.y + ")");
	// }
}
