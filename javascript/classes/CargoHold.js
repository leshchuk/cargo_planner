window.App = window.App || {};

App.CargoHold = function(width, length) {
	this.width = width || 3;
	this.length = length || 7;

	this.el = UTILS.node({
		cls: "hold",
		parent: App.workarea.el
	});
}

App.CargoHold.prototype = {
	init: function(tile) {
		var from = tile;
		var to = App.workarea.tile(from.x + this.width - 1, from.y + this.length - 1);

		this.el.style.top = from.el.offsetTop + "px";
		this.el.style.left = from.el.offsetLeft + "px";
		this.el.style.width = to.el.offsetLeft - from.el.offsetLeft + to.el.clientWidth + "px";
		this.el.style.height = to.el.offsetTop - from.el.offsetTop + to.el.clientHeight + "px";

		for (var x = from.x; x <= to.x; x++) {
			for (var y = from.y; y <= to.y; y++) {
				App.workarea.tile(x, y).hold = true;
			}
		}
	}
}