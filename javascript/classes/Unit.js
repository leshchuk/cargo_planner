window.App = App || {};

App.Unit = function(width, length) {
    this.width = width || 1;
    this.length = length || 1;
    this.index = App.workarea.units.length + 1;

    this.el = UTILS.node({
        cls: "cargo-unit",
        dataset: {index: this.index},
        style: {zIndex: 1000 + this.index},
        parent: document.getElementById("workarea")
    });

    var x1 = App.hold.width + 2;
    var x2 = App.workarea.tiles[0].length - this.width - 1;
    var x = UTILS.random(x1, x2);
    var y = UTILS.random(1, 11 - this.height);
    var tile = App.workarea.tile(x, y);
    this.placeByTile(tile);

    this.plugins = {
        draggable: new App.plugin.Draggable({
            element: this.el,
            container: App.workarea,
            validator: this.ddValid,
            startDrag: this.startDrag,
            success: this.place,
            reject: this.ddReject,
            scope: this
        })
    };
};

App.Unit.prototype = {
    startDrag: function(pos) {
        var tiles = this.unitTilesByPos(pos);

        tiles.forEach(function(t) {
            t.occupied = false;
        });
    },

    ddValid: function(pos) {

        Array.from(App.workarea.el.getElementsByClassName("valid_tile")).forEach(function(el) {
            el.classList.remove("valid_tile");
        });

        var tiles = this.unitTilesByPos(pos);
        var hold = tiles[0].hold;

        for (var i = 0; i < tiles.length; i++) {
            var t = tiles[i];
            if (hold !== t.hold || t.occupied) return false;
        }

        tiles.forEach(function(t) {
            t.el.classList.add("valid_tile");
        });

        return true;
    },

    ddReject: function(pos) {
        // var t = App.workarea.tileByPos(pos);
        this.el.style.left = pos.x + "px";
        this.el.style.top = pos.y + "px";
    },

    place: function(pos) {
        var t = App.workarea.tileByPos(pos);

        Array.from(App.workarea.el.getElementsByClassName("hold_cell")).forEach(function(el) {
            el.classList.remove("valid_tile");
        });

        this.placeByTile(t);
        this.tiles = this.unitTilesByPos(pos);//.forEach(function(t) {t.occupied = true;});
        this.placed = true;
    },

    placeByTile: function(tile) {
        this.el.style.width = tile.el.clientWidth * this.width + "px";
        this.el.style.height = tile.el.clientHeight * this.length + "px";
        this.el.style.left = tile.el.offsetLeft + "px";
        this.el.style.top = tile.el.offsetTop + "px";
    },

    unitTilesByPos: function(pos) {
        var tiles = [];
        var t = App.workarea.tileByPos(pos);
        var x1 = t.x,
            x2 = t.x + this.width,
            y1 = t.y,
            y2 = t.y + this.length;
        for (var x = x1; x < x2; x++) {
            for (var y = y1; y < y2; y++) {
                tiles.push(App.workarea.tile(x, y));
            }
        }
        return tiles;
    },

    set placed(state) {
        this._placed = UTILS.bool(state);
        this.el.classList[state === true ? "add" : "remove"]("placed");
        (this.tiles || []).forEach(function(t) {t.occupied = state;});
    },
    get placed() {
        return UTILS.bool(this._placed);
    }
}